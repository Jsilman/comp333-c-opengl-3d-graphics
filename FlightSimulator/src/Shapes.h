/*
 * Shapes.h
 *
 *  Created on: 11/04/2013
 *      Author: Tim
 */

#ifndef SHAPES_H_
#define SHAPES_H_

void torus(double r, double R, int nsides, int rings);
void cube(double size, int texTiling);

#endif /* SHAPES_H_ */
