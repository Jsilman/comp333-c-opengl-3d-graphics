#ifndef VECTOR3_
#define VECTOR3_

struct Vector3 {

	float x, y, z;

	Vector3(){
		x = 0;
		y = 0;
		z = 0;
	}

	Vector3(float nx, float ny, float nz){
		x = nx;
		y = ny;
		z = nz;
	}

	void operator = (const Vector3 &other){
		x = other.x;
		y = other.y;
		z = other.z;
	}

	Vector3 operator +(const Vector3 &other) const{
		return Vector3(other.x + x, other.y + y, other.z + z);
	}

	void operator += (const Vector3 &other){
		x += other.x;
		y += other.y;
		z += other.z;
	}

	bool operator == (const Vector3 &other) const {
		return (x == other.x && y == other.y && z == other.z);
	}

	bool nearby(const Vector3 &other, float tolerance){

		if (x > other.x - tolerance && x < other.x + tolerance){
			if (y > other.y - tolerance && y < other.y + tolerance){
				if (z > other.z - tolerance && z < other.z + tolerance){
					return true;
				}
			}
		}

		return false;
	}

};

#endif

