/** Functions for drawing shapes like a torus and a cube.
    Replaces ones from glut that don't generate texture coordinates
    Written by Tim Lambert based on code in glut
 */


/* Some of this code was copied from glut and hence we have to include
   the notice below */

/* Copyright (c) Mark J. Kilgard, 1994, 1997. */

/**
(c) Copyright 1993, Silicon Graphics, Inc.

ALL RIGHTS RESERVED

Permission to use, copy, modify, and distribute this software
for any purpose and without fee is hereby granted, provided
that the above copyright notice appear in all copies and that
both the copyright notice and this permission notice appear in
supporting documentation, and that the name of Silicon
Graphics, Inc. not be used in advertising or publicity
pertaining to distribution of the software without specific,
written prior permission.

THE MATERIAL EMBODIED ON THIS SOFTWARE IS PROVIDED TO YOU
"AS-IS" AND WITHOUT WARRANTY OF ANY KIND, EXPRESS, IMPLIED OR
OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  IN NO
EVENT SHALL SILICON GRAPHICS, INC.  BE LIABLE TO YOU OR ANYONE
ELSE FOR ANY DIRECT, SPECIAL, INCIDENTAL, INDIRECT OR
CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER,
INCLUDING WITHOUT LIMITATION, LOSS OF PROFIT, LOSS OF USE,
SAVINGS OR REVENUE, OR THE CLAIMS OF THIRD PARTIES, WHETHER OR
NOT SILICON GRAPHICS, INC.  HAS BEEN ADVISED OF THE POSSIBILITY
OF SUCH LOSS, HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
ARISING OUT OF OR IN CONNECTION WITH THE POSSESSION, USE OR
PERFORMANCE OF THIS SOFTWARE.

US Government Users Restricted Rights

Use, duplication, or disclosure by the Government is subject to
restrictions set forth in FAR 52.227.19(c)(2) or subparagraph
(c)(1)(ii) of the Rights in Technical Data and Computer
Software clause at DFARS 252.227-7013 and/or in similar or
successor clauses in the FAR or the DOD or NASA FAR
Supplement.  Unpublished-- rights reserved under the copyright
laws of the United States.  Contractor/manufacturer is Silicon
Graphics, Inc., 2011 N.  Shoreline Blvd., Mountain View, CA
94039-7311.

OpenGL(TM) is a trademark of Silicon Graphics, Inc.
 */
#include <math.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glut.h>
#include "Shapes.h"

void torus(double r, double R, int nsides, int rings){
	int i, j;
	double theta, phi, theta1;
	double cosTheta, sinTheta;
	double cosTheta1, sinTheta1;
	double ringDelta, sideDelta;
	double s,s1,t,sDelta,tDelta;

	ringDelta = 2.0 * M_PI / rings;
	sideDelta = 2.0 * M_PI / nsides;
	sDelta = 1.0 / rings;
	tDelta = 1.0 / nsides;

	theta = 0.0;
	cosTheta = 1.0;
	sinTheta = 0.0;
	s = 0.0;
	for (i = rings - 1; i >= 0; i--) {
		theta1 = theta + ringDelta;
		s1 = s + sDelta;
		cosTheta1 = cos(theta1);
		sinTheta1 = sin(theta1);
		glBegin(GL_QUAD_STRIP);
		phi = 0.0;
		t = 0.0;
		for (j = nsides; j >= 0; j--) {
			double cosPhi, sinPhi, dist;

			phi += sideDelta;
			t += tDelta;
			cosPhi = cos(phi);
			sinPhi = sin(phi);
			dist = R + r * cosPhi;

			glNormal3d(cosTheta1 * cosPhi, -sinTheta1 * cosPhi, sinPhi);
			glTexCoord2d(s1,t);
			glVertex3d(cosTheta1 * dist, -sinTheta1 * dist, r * sinPhi);
			glNormal3d(cosTheta * cosPhi, -sinTheta * cosPhi, sinPhi);
			glTexCoord2d(s,t);
			glVertex3d(cosTheta * dist, -sinTheta * dist,  r * sinPhi);
		}
		glEnd();
		theta = theta1;
		s = s1;
		cosTheta = cosTheta1;
		sinTheta = sinTheta1;
	}
}


static float n[6][3] =
{
		{-1.0f, 0.0f, 0.0f},
		{0.0f, 1.0f, 0.0f},
		{1.0f, 0.0f, 0.0f},
		{0.0f, -1.0f, 0.0f},
		{0.0f, 0.0f, 1.0f},
		{0.0f, 0.0f, -1.0f}
};
static int faces[6][4] =
{
		{0, 1, 2, 3},
		{3, 2, 6, 7},
		{7, 6, 5, 4},
		{4, 5, 1, 0},
		{5, 6, 2, 1},
		{7, 4, 0, 3}
};

void cube(double size, int texTiling){
	float v[8][3];
	int i;

	v[0][0] = v[1][0] = v[2][0] = v[3][0] = (float)-size / 2;
	v[4][0] = v[5][0] = v[6][0] = v[7][0] = (float)size / 2;
	v[0][1] = v[1][1] = v[4][1] = v[5][1] = (float)-size / 2;
	v[2][1] = v[3][1] = v[6][1] = v[7][1] = (float)size / 2;
	v[0][2] = v[3][2] = v[4][2] = v[7][2] = (float)-size / 2;
	v[1][2] = v[2][2] = v[5][2] = v[6][2] = (float)size / 2;

	for (i = 5; i >= 0; i--) {
		glBegin(GL_POLYGON);
		glNormal3fv(n[i]);
		glTexCoord2f(0,0);
		glVertex3fv(v[faces[i][0]]);
		glTexCoord2f(texTiling,0);
		glVertex3fv(v[faces[i][1]]);
		glTexCoord2f(texTiling,texTiling);
		glVertex3fv(v[faces[i][2]]);
		glTexCoord2f(0,texTiling);
		glVertex3fv(v[faces[i][3]]);
		glEnd();
	}
}

