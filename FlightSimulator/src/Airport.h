/*
 * Airport.h
 *
 *  Created on: May 15, 2017
 *      Author: Jack
 */

#ifndef AIRPORT_H_
#define AIRPORT_H_

class Airport {
public:
	Airport();
	void draw();
};

#endif /* AIRPORT_H_ */
