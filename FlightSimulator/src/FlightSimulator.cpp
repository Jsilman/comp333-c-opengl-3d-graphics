#include <iostream>
#include <stdlib.h>
#include <string>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <vector>
#include <GL/glut.h>
#include <GL/glu.h>
#include "Plane.h"
#include "Airport.h"
#include "Math.h"
#include "SOIL.h"
#include "UtilityStructs.h"

//Animation FPS
#define ANIMATION_STEP (1000 / 60)

//PI, Degree to Radian and Radian to degree
#define PI 3.1415926
#define D2R PI/180
#define R2D 180/PI

//Texture
#define MAX_NO_TEXTURES 1

//Scene Components
bool isSimulatorStarted = false;
Plane titlePlane = Plane(0,0,1);
Plane plane = Plane(-37.5,1.75,0);
Airport airport = Airport();

//Window Dimensions
int WINDOW_WIDTH = 1200;
int WINDOW_HEIGHT = 900;

// Four view ports (Main and 3 smaller)
int viewportOffset = 3;
float viewports[4][4] = {
		{0, WINDOW_HEIGHT/2, WINDOW_WIDTH, WINDOW_HEIGHT/2},
		{WINDOW_WIDTH/3 * 2, 0, WINDOW_WIDTH/3, WINDOW_HEIGHT/2},
		{WINDOW_WIDTH/3, 0, WINDOW_WIDTH/3, WINDOW_HEIGHT/2},
		{0, 0, WINDOW_WIDTH/3, WINDOW_HEIGHT/2}
};

// Taken from Tim Lambert's "textures" example project, 2013.
// Loads Bitmaps And Convert To Textures
GLuint texture[MAX_NO_TEXTURES];
int loadGLTexture(const char *fileName)
{
	std::string name = "textures/";
	name += fileName;
	// load an image file from texture directory as a new OpenGL texture
	texture[0] = SOIL_load_OGL_texture
			(
					name.c_str(),
					SOIL_LOAD_AUTO,
					SOIL_CREATE_NEW_ID,
					SOIL_FLAG_INVERT_Y
			);

	if(texture[0] == 0) {
		std::cerr << fileName << " : " << SOIL_last_result();
		return false;
	}

	// Typical Texture Generation Using Data From The Bitmap
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
	return true;// Return Success
}

void init(void){
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	//Load textures
	loadGLTexture("sky.jpg");
	loadGLTexture("panel.bmp");
	loadGLTexture("ground.png");
	loadGLTexture("metal.jpg");

}

void setupFirstPersonView(){

	// Set view port
	int i = (0 + viewportOffset) % 4;
	glViewport(viewports[i][0], viewports[i][1],
			   viewports[i][2], viewports[i][3]);

	// Set perspective based on view port ratio
	glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90, viewports[i][2] / viewports[i][3], 0.1, 2000);
    glMatrixMode(GL_MODELVIEW);

    //Camera Eye Position
    Vector3 eye;
	eye.x = plane.posX + cos(-plane.rotY * D2R) * cos(plane.rotZ * D2R);
	eye.y = plane.posY + sin(plane.rotZ * 3.14/180);
	eye.z = plane.posZ + sin(-plane.rotY * D2R) * cos(plane.rotZ * D2R);

	//Camera Look At position
	Vector3 look;
	look.x = plane.posX + cos(-plane.rotY * D2R) * cos(plane.rotZ * D2R) * 2;
	look.y = plane.posY + sin(plane.rotZ * D2R) * 2;
	look.z = plane.posZ + sin(-plane.rotY * D2R) * cos(plane.rotZ * D2R) * 2;

	//Up Vector (Only y needs to be set)
	Vector3 up;
	up.y = cos(plane.rotZ * 3.14/180);

	// Construct projection matrix (Using the Z rotation as an up vector to
	// prevent the camera from turning upside down while performing a roll)
	gluLookAt(
			eye.x, eye.y, eye.z,
			look.x, look.y, look.z,
			up.x, up.y, up.z
			);

}

void setupThirdPersonView(float cameraDistance){

	// Set view port
	int i = (1 + viewportOffset) % 4;
	glViewport(viewports[i][0], viewports[i][1],
			   viewports[i][2], viewports[i][3]);

	// Set perspective based on view port ratio
	glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90,  viewports[i][2] /  viewports[i][3], 0.1, 2000);
    glMatrixMode(GL_MODELVIEW);

    Vector3 cam;
    cam.x = 30;
    cam.y = 25;
    cam.z = 30;

	gluLookAt(cam.x, cam.y, cam.z,
			  0, 0, 0,
			  0, 1, 0);
}

void setupTopDownView(float cameraHeight){

	// Set view port
	int i = (2 + viewportOffset) % 4;
	glViewport(viewports[i][0], viewports[i][1],
			   viewports[i][2], viewports[i][3]);


	// Top down camera uses orthographic project with the view port
	// ratio to avoid distortion
	glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-viewports[i][2] / 10, viewports[i][2] / 10,
    		-viewports[i][3] / 10, viewports[i][3] / 10,
			-50.0, 50.0);
    glMatrixMode(GL_MODELVIEW);

	gluLookAt(0, cameraHeight, 0,
			  0, 0, 0,
			  0, 0, 1);
}

void setupSideView(float cameraDistance){

	// Set view port
	int i = (3 + viewportOffset) % 4;
	glViewport(viewports[i][0], viewports[i][1],
			   viewports[i][2], viewports[i][3]);

	// Set perspective based on view port ratio
	glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90, viewports[i][2] / viewports[i][3], 0.1, 2000);
    glMatrixMode(GL_MODELVIEW);

    Vector3 eye;
    eye.x = plane.posX + cos((plane.rotY + 90.0f) * D2R) * cameraDistance;
    eye.y = plane.posY + cameraDistance / 1.5;
    eye.z = plane.posZ + sin((plane.rotY - 90.0f) * D2R) * cameraDistance;

	gluLookAt(eye.x, eye.y, eye.z,
			  plane.posX, plane.posY, plane.posZ,
			  0, 1, 0);
}

//Utility function that draws a bitmap string
void drawBitmapString(char message[], float x, float y){
    int len = strlen(message);
    glColor3f(1,1,1);
    glPushMatrix();
		glRasterPos2i(x, y);
		for ( int i = 0; i < len; i++ ){
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, message[i]);
		}
    glPopMatrix();
}

void drawTitleScreen(){

	int centerWidth = WINDOW_WIDTH/2;

	//Menu Strings
	char title[] = "Flight Simulator";
	char name[] = "Jack Silman - 44767609";

	char item1[] = "[s] Start Simulator";

	char item8[] = "Commands available after starting simulator";
	char item2[] = "[1] Start take off and enter the holding pattern";
	char item3[] = "[2] Do a cork screw (while in the holding pattern)";
	char item4[] = "[3] Do a loop (while in the holding pattern)";
	char item5[] = "[4] Start landing after the current holding pattern completes";

	char item6[] = "[r] Rotate view ports";
	char item7[] = "[q] Quit application";

	//Setup for 2d drawing
	glClearColor(0.25, 0.25, 0.70, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, WINDOW_WIDTH, 0, WINDOW_HEIGHT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//Draw all the strings
	drawBitmapString(title, centerWidth - strlen(title)/2 * 10, 800);
	drawBitmapString(name, centerWidth - strlen(name)/2 * 10, 770);

	drawBitmapString(item1, 150, 220);
	drawBitmapString(item6, 150, 190);
	drawBitmapString(item7, 150, 160);

	drawBitmapString(item8, 500, 220);
	drawBitmapString(item2, 500, 190);
	drawBitmapString(item3, 500, 160);
	drawBitmapString(item4, 500, 130);
	drawBitmapString(item5, 500, 100);

	//Setup for 3d drawing (The plane)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90, 64/48.0, 0.1, 2000);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0, 5, 0,
			  0, 0, 0,
			  0, 0, 1);

	titlePlane.rotX = 90;
	titlePlane.draw();
}

void drawScene(){

	//Draw Scene Lighting
	float lightPosition[] = {0.0f, 10.0f, 0.0f, 1.0f};
	float lightAmbient[] =  {0.7f, 0.7f, 0.7f, 0.0f};
	float lightDiffuse[] =  {0.3f, 0.3f, 0.3f, 0.0f};
	float lightSpecular[] = {0.0f, 0.0f, 0.0f, 0.0f};

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	//Draw Scene objects
	airport.draw();
	plane.draw();

}

void displayCB(void){

	// If the simulation has started draw the views, otherwise we draw the
	// title screen and wait until user input
	if (isSimulatorStarted){

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		setupThirdPersonView(4);
		drawScene();

		setupFirstPersonView();
		drawScene();

		setupSideView(5);
		drawScene();

		setupTopDownView(40);
		drawScene();

	} else {
		drawTitleScreen();
	}

	glutSwapBuffers();
}


void keyCB(unsigned char key, int x, int y){

	if (key =='q'){
		exit(0);
	} else if (key == 's'){
		isSimulatorStarted = true;
	}

	//Once the simulator has started accept user initiated animations
	if (isSimulatorStarted){
		if (key == '1'){
			plane.takeOff();
		}else if (key == '2'){
			plane.corkScrew();
		} else if (key == '3'){
			plane.loop();
		} else if (key == '4'){
			plane.land();
		} else if (key == 'r'){
			viewportOffset++;
		}
	}

}

void timerCB(int v) {

	int time = glutGet(GLUT_ELAPSED_TIME);
	glutTimerFunc(ANIMATION_STEP, timerCB, time);

	//Update the plane
	int delta_t = time - v;
	plane.update(delta_t);

	//Rotate the title plane manually
	titlePlane.rotZ -= 0.3f;
	titlePlane.propRotation += 5.0f;

	glutPostRedisplay();

}

void reshapeCB(int w, int h){

	WINDOW_HEIGHT = h;
	WINDOW_WIDTH = w;

	//Update viewports
	float newViewports[4][4] = {
			{0, WINDOW_HEIGHT/2, WINDOW_WIDTH, WINDOW_HEIGHT/2},
			{WINDOW_WIDTH/3 * 2, 0, WINDOW_WIDTH/3, WINDOW_HEIGHT/2},
			{WINDOW_WIDTH/3, 0, WINDOW_WIDTH/3, WINDOW_HEIGHT/2},
			{0, 0, WINDOW_WIDTH/3, WINDOW_HEIGHT/2}
	};

	memcpy(viewports, newViewports, sizeof(viewports));

}

int main(int argc, char *argv[]) {

	// Initialize GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

	// Setup the window
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutInitWindowPosition(25,25);
	glutCreateWindow("Flight Simulator");

	// Register callback functions
	glutDisplayFunc(displayCB);
	glutKeyboardFunc(keyCB);
	glutTimerFunc(ANIMATION_STEP, timerCB, 0);
	glutReshapeFunc(reshapeCB);

	// Initialize application and begin looping
    init();
	glutMainLoop();
	return 0;

}
