/*
 * Plane.h
 *
 *  Created on: May 13, 2017
 *      Author: Jack
 */
#include "UtilityStructs.h"

#ifndef PLANE_H_
#define PLANE_H_

class Plane {
public:
	Plane(float x, float y, float z);

	float posX, posY, posZ;
	float rotX, rotY, rotZ;
	float propRotation, propRotationSpeed;
	float wheelRotation, wheelRotationSpeed;

	bool isPropellerMoving;
	bool isWheelsMoving;
	bool isPerformingLoop;
	bool isPerformingCorkscrew;

	float rollAngle;

	void draw();
	void update(int delta_t);
	void corkScrew();
	void loop();
	void takeOff();
	void land();

	private:
	void orbitAround(float x, float z, float* angle,float orbitSpeed, float radius);
	void roll(float x, float y);
	void moveTo(Vector3 location, float moveSpeed);

};

#endif /* PLANE_H_ */
