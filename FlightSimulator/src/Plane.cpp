/*
 * Plane.cpp
 * Handles the plane drawing and updating (Animation)
 *  Created on: May 8, 2017
 *      Author: Jack
 */

#include "Plane.h"
#include <GL/glut.h>
#include "Math.h"
#include "Shapes.h"
#include <iostream>

//PI, Degree to Radian and Radian to degree
#define PI 3.1415926
#define D2R PI/180
#define R2D 180/PI

enum AnimationState {
		RUNWAY_TAKEOFF,
		TAKEOFF,
		HOLDING_PATTERN,
		LANDING,
		RUNWAY_LANDING,
		NO_STATE,
		ENTER_HOLDING_PATTERN,
		ENTERED_HOLDING_PATTERN,
		EXIT_HOLDING_PATTERN,
		EXITED_HOLDING_PATTERN
	};

enum AnimationCommand {
		START_TAKEOFF,
		START_LANDING,
		ORBIT,
		NO_COMMAND
	};

AnimationState animationState;
AnimationCommand animationCommand;

// Angles that keep track of various orbits
float orbitAngle = 0.0f;
float landingOrbitAngle = (-90.0f * D2R);
float takeoffOrbitAngle = (-90.0f * D2R);

Plane::Plane(float x, float y, float z) {
	posX = x;
	posY = y;
	posZ = z;
	rotX = rotY = rotZ = 0;
	propRotation = wheelRotation = 0;
	propRotationSpeed = 5.0f;
	wheelRotationSpeed = 2.0f;
	animationCommand = NO_COMMAND;
	animationState = NO_STATE;
	isPerformingCorkscrew = false;
	isPerformingLoop = false;
	isPropellerMoving = false;
	isWheelsMoving = false;
	rollAngle = 0.0f;
}

void Plane::draw(){

	glPushMatrix();

	// Position the plane
	glTranslatef(posX, posY, posZ);
	glRotatef(rotY, 0, 1, 0);
	glRotatef(rotZ, 0, 0, 1);
	glRotatef(rotX, 1, 0, 0);

	//Draw the plane
	glPushMatrix();

		//Plane Body
		glPushMatrix();
			glTranslatef(0, 1.0f  -1.7f,0);
			glScalef(4, 0.75, 0.75);
			glColor3f(0.8, 0.2 ,0.2);
			cube(1,1);
		glPopMatrix();

		//Plane Wing
		glPushMatrix();
			glTranslatef(1.4, 0.67f  -1.7f,0);
			glScalef(1.0, 0.1, 3);
			glColor3f(0.8, 0.2 ,0.2);
			cube(1,1);
		glPopMatrix();

		//Plane Wing
		glPushMatrix();
			glTranslatef(1.4, 2.0f  -1.7f,0);
			glScalef(1.0, 0.1, 3);
			glColor3f(0.8, 0.2 ,0.2);
			cube(1,1);
		glPopMatrix();

		//Plane Wing Support
		glPushMatrix();
			glTranslatef(1.4, 1.35f  -1.7f,-0.7);
			glScalef(0.1, 1.35, 0.1);
			glColor3f(0.9, 0.9 ,0.9);
			cube(1,1);
		glPopMatrix();

		//Plane Wing Support
		glPushMatrix();
			glTranslatef(1.4, 1.35f  -1.7f,-1);
			glRotatef(25,1,0,0);
			glScalef(0.1, 1.35, 0.1);
			glColor3f(0.9, 0.9 ,0.9);
			cube(1,1);
		glPopMatrix();

		//Plane Wing Support
		glPushMatrix();
			glTranslatef(1.4, 1.35f  -1.7f,-1.3);
			glScalef(0.1, 1.35, 0.1);
			glColor3f(0.9, 0.9 ,0.9);
			cube(1,1);
		glPopMatrix();

		//Plane Wing Support
		glPushMatrix();
			glTranslatef(1.4, 1.35f  -1.7f,0.7);
			glScalef(0.1, 1.35, 0.1);
			glColor3f(0.9, 0.9 ,0.9);
			cube(1,1);
		glPopMatrix();

		//Plane Wing Support
		glPushMatrix();
			glTranslatef(1.4, 1.35f  -1.7f,1.3);
			glScalef(0.1, 1.35, 0.1);
			glColor3f(0.9, 0.9 ,0.9);
			cube(1,1);
		glPopMatrix();

		//Plane Wing Support
		glPushMatrix();
			glTranslatef(1.4, 1.35f  -1.7f,1);
			glRotatef(-25,1,0,0);
			glScalef(0.1, 1.35, 0.1);
			glColor3f(0.9, 0.9 ,0.9);
			cube(1,1);
		glPopMatrix();

		//Propeller Support
		glPushMatrix();
			glTranslatef(2,1.0f  -1.7f,0);
			glScalef(0.5, 0.1, 0.1);
			glColor3f(0.6, 0.6 ,0.6);
			cube(1,1);
		glPopMatrix();

		//Propeller Blade 1
		glPushMatrix();
			glTranslatef(2.1,1.0f  -1.7f,0);
			glRotatef(propRotation, 1, 0, 0);
			glScalef(0.1, 0.1, 1.5);
			glColor3f(0.7, 0.7 ,0.7);
			cube(1,1);
		glPopMatrix();

		//Propeller Blade 2
		glPushMatrix();
			glTranslatef(2.1,1.0f  -1.7f,0);
			glRotatef(propRotation, 1, 0, 0);
			glScalef(0.1, 1.5, 0.1);
			glColor3f(0.7, 0.7 ,0.7);
			cube(1,1);
		glPopMatrix();

		//Instruments
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 2);
			glPushMatrix();
				glTranslatef(1.45f,1.35f  -1.7f,0);
				glRotatef(25,0,0,1);
				glScalef(0.25, 0.25,0.7);
				glColor3f(1, 1 ,1);
				cube(1,1);
			glPopMatrix();
		glDisable(GL_TEXTURE_2D);

		//Tail
		glPushMatrix();
			glTranslatef(-1.3f, 1.3  -1.7f,0);
			glRotatef(45,0,0,1);
			glScalef(0.7, 0.7, 0.1);
			glColor3f(0.8, 0.2 ,0.2);
			cube(1,1);
		glPopMatrix();

		//Wheels
		glPushMatrix();
			glTranslatef(0.8,.25f -1.7f,-0.5f);
			glRotatef(wheelRotation, 0,0,1);
			glScalef(1,1,1);
			glColor3f(0.0, 0.0 ,0.0);
			glutSolidTorus(0.1,0.2,10,10);
		glPopMatrix();

		//Wheel
		glPushMatrix();
			glTranslatef(0.8,.25f -1.7f,0.5f);
			glRotatef(wheelRotation, 0,0,1);
			glScalef(1,1,1);
			glColor3f(0.0, 0.0 ,0.0);
			glutSolidTorus(0.1,0.2,10,10);
		glPopMatrix();

		//Wheel
		glPushMatrix();
			glTranslatef(-1.5f,.25f -1.7f,0.0f);
			glRotatef(wheelRotation, 0,0,1);
			glScalef(1,1,1);
			glColor3f(0.0, 0.0 ,0.0);
			glutSolidTorus(0.1,0.2,10,10);
		glPopMatrix();

		//Wheel Support
		glPushMatrix();
			glTranslatef(0.8,.25f -1.7f,0.0f);
			glScalef(0.15,0.15,1);
			glColor3f(0.5, 0.5 ,0.5);
			cube(1,1);
		glPopMatrix();

		//Wheel Support
		glPushMatrix();
			glTranslatef(0.8,.5f -1.7f,0.0f);
			glScalef(0.15,0.5,0.15);
			glColor3f(0.5, 0.5 ,0.5);
			cube(1,1);
		glPopMatrix();

		//Wheel Support
		glPushMatrix();
			glTranslatef(-1.5f,.25f -1.7f,0.0f);
			glScalef(0.15,0.15,0.6);
			glColor3f(0.5, 0.5 ,0.5);
			cube(1,1);
		glPopMatrix();

		//Wheel Support
		glPushMatrix();
			glTranslatef(-1.5f,.5f -1.7f,-0.25f);
			glScalef(0.15,0.5,0.15);
			glColor3f(0.5, 0.5 ,0.5);
			cube(1,1);
		glPopMatrix();

		//Wheel Support
		glPushMatrix();
			glTranslatef(-1.5f,.5f -1.7f,0.25f);
			glScalef(0.15,0.5,0.15);
			glColor3f(0.5, 0.5 ,0.5);
			cube(1,1);
		glPopMatrix();

	glPopMatrix();
	glPopMatrix();
}

void Plane::update(int delta_t){

	//Key Locations (Used for animations)
	Vector3 planeLocation = Vector3(posX, posY, posZ);
	Vector3 startLocation = Vector3(-37.5, 1.75,0);
	Vector3 takeOffStart = Vector3(0, 1.75,0);
	Vector3 takeOffEnd = Vector3(25,15,0);

	Vector3 HoldingPatternEntrance = Vector3(25 * cos(90 * D2R),
											 takeOffEnd.y,
											 25 * sin(90 * D2R));

	Vector3 HoldingPatternExit = Vector3(25 * cos(270 * D2R),
										 takeOffEnd.y,
										 25 * sin(270 * D2R));

	Vector3 LandingPatternEntrance = Vector3(takeOffEnd.x,
											 takeOffEnd.y,
											 takeOffEnd.z - 25);

	Vector3 TakeoffPatternEntrance = Vector3(takeOffEnd.x,
											 takeOffEnd.y,
											 takeOffEnd.z + 25);

	float moveSpeed = 0.1f;

	// If the command is given to start take off then begin the take off
    // routine moving between animation states once the previous one has
	// been reached
	if (animationCommand == START_TAKEOFF){
		if (planeLocation == startLocation){
			animationState = RUNWAY_TAKEOFF;
		} else if (planeLocation == takeOffStart && animationState == RUNWAY_TAKEOFF){
			animationState = TAKEOFF;
		} else if (planeLocation == takeOffEnd && animationState == TAKEOFF){
			animationState = ENTER_HOLDING_PATTERN;
		} else if (planeLocation.nearby(TakeoffPatternEntrance, 2) && animationState == ENTER_HOLDING_PATTERN){
			animationState = ENTERED_HOLDING_PATTERN;
		} else if (planeLocation == HoldingPatternEntrance && animationState == ENTERED_HOLDING_PATTERN){
			animationState = HOLDING_PATTERN;
			animationCommand = NO_COMMAND;
		}
	}

	//Same process but for landing
	if (animationCommand == START_LANDING){
		if (planeLocation.nearby(HoldingPatternExit, 2) &&
			isPerformingLoop == false){
			animationState = EXIT_HOLDING_PATTERN;
		} else if (planeLocation == LandingPatternEntrance &&
				   animationState == EXIT_HOLDING_PATTERN){
			animationState = EXITED_HOLDING_PATTERN;
		} else if (planeLocation.nearby(takeOffEnd, 1) &&
				   animationState == EXITED_HOLDING_PATTERN){
			animationState = LANDING;
		} else if (planeLocation == takeOffStart &&
				   animationState == LANDING){
			animationState = RUNWAY_LANDING;
		} else if (planeLocation == startLocation &&
				   animationState == RUNWAY_LANDING){
			animationCommand = NO_COMMAND;
		}
	}

	//Perform Animation based on the current state (Moving between locations,
	//orbiting and performing stunts)
	if (animationState == RUNWAY_TAKEOFF){

		//Reset angles to default
		rotY = 0;
		orbitAngle = 0.0f;
		landingOrbitAngle = (-90.0f * D2R);
		takeoffOrbitAngle = (-90.0f * D2R);

		moveTo(takeOffStart, moveSpeed);
	} else if (animationState == TAKEOFF){
		moveTo(takeOffEnd, moveSpeed);
	}else if (animationState == ENTER_HOLDING_PATTERN){
		orbitAround(25 , (25/2), &takeoffOrbitAngle, 0.02f, 25/2);
	} else if (animationState == ENTERED_HOLDING_PATTERN){
		rotY = -180;
		orbitAngle = (90 * D2R);
		moveTo(HoldingPatternEntrance, moveSpeed * 2);
	} else if (animationState == HOLDING_PATTERN){

		//Perform a loop (or continue orbiting)
		if (isPerformingLoop){
			if ((rollAngle * R2D) < 360){
				roll(posX, posZ);
			} else {
				isPerformingLoop = false;
				rotZ = rollAngle = 0;
			}
		} else {
			orbitAround(0,0, &orbitAngle,0.02f, 25.0f);
		}

		//Perform a cork screw twirl
		if (isPerformingCorkscrew){
			if (rotX < 360){
				rotX += 2.0f;;
			} else {
				rotX = 0;
				isPerformingCorkscrew = false;
			}
		}

	} else if (animationState == EXIT_HOLDING_PATTERN){

		orbitAngle = (-90 * D2R);
		moveTo(LandingPatternEntrance, moveSpeed * 2);
		rotY = 0;

		//Finish the cork screw if its still going
		if (isPerformingCorkscrew){
			if (rotX < 360){
				rotX += 2.0f;
			} else {
				rotX = 0;
				isPerformingCorkscrew = false;
			}
		}
	} else if (animationState == EXITED_HOLDING_PATTERN){
		orbitAround(25 , (-25/2), &landingOrbitAngle,0.02f, 25/2);
		rotX = 0;
	} else if (animationState == LANDING){
		rotY = 180;
		moveTo(takeOffStart, moveSpeed);
	} else if (animationState == RUNWAY_LANDING){
		moveTo(startLocation, moveSpeed);
	}

	// Rotate the propeller
	if (!(planeLocation == startLocation)){
		propRotation += propRotationSpeed;
	}

	// Rotate the wheels
	if (animationState == RUNWAY_LANDING ||
		animationState == RUNWAY_TAKEOFF){
		wheelRotation += wheelRotationSpeed;
	}

}

void Plane::moveTo(Vector3 location, float moveSpeed){

	float x = location.x;
	float y = location.y;
	float z = location.z;

	//If we are not already at the position
	if (!(x == posX && y == posY && z == posZ)){

		// Calculate the movement vector
		float vecX = x - posX;
		float vecY = y - posY;
		float vecZ = z - posZ;

		// Calculate the magnitude of the vector and unit vector
		float magnitude = sqrt(vecX*vecX + vecY*vecY + vecZ*vecZ);
		vecX /= magnitude;
		vecY /= magnitude;
		vecZ /= magnitude;

		// If the magnitude is less than a single step (move speed) then
		// just set the position to the destination, otherwise increment
		// the position by the movement speed * the unit vector
		if (magnitude < moveSpeed){
			posX = x;
			posY = y;
			posZ = z;
		} else {
			posX += moveSpeed * vecX;
			posY += moveSpeed * vecY;
			posZ += moveSpeed * vecZ;
		}

	}

}

void Plane::takeOff(){
	if (animationCommand == NO_COMMAND && animationState != HOLDING_PATTERN){
		animationCommand = START_TAKEOFF;
	}
}

void Plane::land(){
	if (animationCommand == NO_COMMAND){
		animationCommand = START_LANDING;
	}
}

void Plane::loop(){
	if (animationState == HOLDING_PATTERN){
		isPerformingLoop = true;
	}
}

void Plane::corkScrew(){
	if (animationState == HOLDING_PATTERN){
		isPerformingCorkscrew = true;
	}
}


void Plane::orbitAround(float x, float z, float* angle, float speed, float r){

	//Store the original position
	float originalPosX = posX;
	float originalPosZ = posZ;

	//Calculate the X, Y positions
	posX = x + r * cos(*angle);
	posZ = z + r * sin(*angle);

	//Calculate the heading using the original and new positions
	rotY = -(atan2(posZ - originalPosZ ,posX - originalPosX) * R2D);

	//Update the Angle
	*angle += speed / (r / 10);
}

void Plane::roll(float x, float z){

	float rollRadius = 5;
	float orbitHeight = 10;

	posX += 0.04f * cos(-rotY * D2R) * (rollRadius * cos(rollAngle));
	posY = orbitHeight + (rollRadius * cos(rollAngle));
	posZ += 0.04f * sin(-rotY * D2R) * (rollRadius * cos(rollAngle));

	rotZ = -(rollAngle * R2D);
	rollAngle += 0.02f;

}

