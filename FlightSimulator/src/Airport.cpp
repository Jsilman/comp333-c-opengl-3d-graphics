/*
 * Airport.cpp
 *  Handles the drawing of the airport
 *  Created on: May 15, 2017
 *      Author: Jack
 */

#include "Airport.h"
#include <GL/glut.h>
#include "Math.h"
#include "time.h"
#include "shapes.h"

Airport::Airport() {

}

void drawRunway(float x, float y, float z){

	//Run way
	glPushMatrix();
		glTranslatef(x, y, z);
		glScalef(70, 0.1, 5);
		glColor3f(0.3, 0.3 ,0.3);
		cube(1,1);
	glPopMatrix();

	//Run way stripes

	glPushMatrix();
		glTranslatef(-70.0f/2 + x, 0.1 + y, 5/2.0f + z);
		glScalef(70, 1, 0.2f);
		glColor3f(1.0f, 1.0f ,1.0f);
		glBegin(GL_QUADS);
			glVertex3f(1,0,0);
			glVertex3f(1,0,1);
			glVertex3f(0,0,1);
			glVertex3f(0,0,0);
		glEnd();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-70.0f/2 + x, 0.1 + y, -5/2.0f + z);
		glScalef(70, 1, 0.2f);
		glColor3f(1.0f, 1.0f ,1.0f);
		glBegin(GL_QUADS);
			glVertex3f(1,0,0);
			glVertex3f(1,0,1);
			glVertex3f(0,0,1);
			glVertex3f(0,0,0);
		glEnd();
	glPopMatrix();

	int count = 0;

	for (count = 0; count < 25; count++){
		glPushMatrix();
			glTranslatef(-3 * count + 70.0f/2 - 3 + x, 0.1 + y, -0.1f + z);
			glScalef(2, 1, 0.2f);
			glColor3f(1.0f, 1.0f ,1.0f);
			glBegin(GL_QUADS);
				glVertex3f(1,0,0);
				glVertex3f(1,0,1);
				glVertex3f(0,0,1);
				glVertex3f(0,0,0);
			glEnd();
		glPopMatrix();
	}

}

void drawTree(float x, float y, float z){

	//Trunk
	glPushMatrix();
		glTranslatef(x, y, z);
		glScalef(0.5, 5, 0.5);
		glColor3f(0.6f, 0.4f ,0.2f);
		glutSolidCube(1);
	glPopMatrix();

	//Leaves
	glPushMatrix();
		glTranslatef(x, y + 1.5, z);
		glRotatef(-90,1,0,0);
		glColor3f(0.3f, 0.6f ,0.2f);
		glutSolidCone(1,2,8,16);
	glPopMatrix();

}

void drawHanger(float x, float y, float z){

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 4);

	glPushMatrix();
		glTranslatef(x, y + 2, z);
		glScalef(7.5, 0.1, 7.5);
		glColor3f(0.7f, 0.7f ,0.7f);
		cube(1,2);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(x - 7.5/2, y, z);
		glScalef(0.1, 4, 7.5);
		glColor3f(0.7f, 0.7f ,0.7f);
		cube(1,2);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(x, y, z + 7.5/2);
		glScalef(7.5, 4, 0.1);
		glColor3f(0.7f, 0.7f ,0.7f);
		cube(1,2);
	glPopMatrix();

	glPushMatrix();
		glTranslatef(x, y, z - 7.5/2);
		glScalef(7.5, 4, 0.1);
		glColor3f(0.7f, 0.7f ,0.7f);
		cube(1,2);
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);

	glPushMatrix();
		glTranslatef(x, y - 2, z);
		glScalef(7.5, 0.1, 7.5);
		glColor3f(0.3, 0.3 ,0.3);
		cube(1,2);
	glPopMatrix();
}


void Airport::draw(){

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 3);

	//Ground
	glPushMatrix();
		glTranslatef(0,0,0);
		glScalef(500, 0.01, 500);
		glColor3f(1, 1 ,1);
		cube(1,25);
	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, 1);

	//Sky Box
	glPushMatrix();
		glTranslatef(0,0,0);
		glScalef(1000, 1000, 1000);
		glColor3f(0.9, 0.9 ,0.9);
		cube(1,1);
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);

	//Draw Hangar
	drawHanger(-75/2 - 1.8, 2, 0);

	//Draw Runway
	drawRunway(0, 0.05, 0);

	int x = 0;

	//Draw Trees
	for (x = -32; x <= 32; x+=4){
		drawTree(x, 0, 3.75);
	}

	x = 0;
	for (x = -32; x <= 32; x+=4){
		drawTree(x, 0, -3.75);
	}

}



